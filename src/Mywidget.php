<?php

namespace yourock\mywidget;

class Mywidget extends \yii\base\Widget
{
	public $message;
	public $name;

	public function init()
	{
		parent::init();

		if ($this->message === null) {
			$this->message = 'Hello World';
		}

		if ($this->name === null) {
			$this->name = 'username';
		}
	}

	public function run()
	{
		// подключаем к виджету view
		return $this->render('index', ['message' => $this->message, 'name' =>$this->name]);
	}
}